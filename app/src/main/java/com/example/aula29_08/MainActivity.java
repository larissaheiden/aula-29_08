package com.example.aula29_08;

import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnSalvar;
    private TextView textView;
    private TextInputEditText editText;

    private static final String Preferencias_file = "Notas";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSalvar = findViewById(R.id.btnSalvar);
        editText = findViewById(R.id.textInput);
        textView = findViewById(R.id.textResultado);

        preferences = getSharedPreferences(Preferencias_file, MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("teste", "BabyShark thururu babyshark");
        editor.commit();
        Toast.makeText(this, preferences.getString("teste", "Erro ao recuperar String"), Toast.LENGTH_LONG).show();





    }
}
